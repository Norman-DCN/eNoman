<?php
/**
 * Created by PhpStorm.
 * User: Gebruiker
 * Date: 25/05/2018
 * Time: 14:01
 */

Class ProductController extends Controller
{

    public function index($slug)
    {
        $products = db()->query('SELECT * FROM products WHERE slug = :slug', ['slug' => $slug])
            ->first('Product');
        if ($products) {
            return response()->view('single-product')
                ->with('product', $products);
        } else {
            return response()->view('error/404');

        }
    }

    public function products()
    {
        $products = db()->query('SELECT * FROM products')
            ->select('Product');

        return response()->view('shop')
            ->with('products', $products);
    }
}
