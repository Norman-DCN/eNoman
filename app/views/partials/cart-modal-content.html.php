<div class="modal-header">
    <h1>Cart</h1>
    <a class="close-modal get-cart" data-url="<?php echo router()->name('cart.get'); ?>" data-dismiss="modal"
       aria-label="Close">
        <svg viewBox="0 0 20 20">
            <path fill="#000000"
                  d="M15.898,4.045c-0.271-0.272-0.713-0.272-0.986,0l-4.71,4.711L5.493,4.045c-0.272-0.272-0.714-0.272-0.986,0s-0.272,0.714,0,0.986l4.709,4.711l-4.71,4.711c-0.272,0.271-0.272,0.713,0,0.986c0.136,0.136,0.314,0.203,0.492,0.203c0.179,0,0.357-0.067,0.493-0.203l4.711-4.711l4.71,4.711c0.137,0.136,0.314,0.203,0.494,0.203c0.178,0,0.355-0.067,0.492-0.203c0.273-0.273,0.273-0.715,0-0.986l-4.711-4.711l4.711-4.711C16.172,4.759,16.172,4.317,15.898,4.045z"></path>
        </svg>
    </a>
</div>
<div class="modal-body">
    <table cellspacing="0" class="shop_table cart">
        <thead>
        <tr>
            <th class="product-thumbnail">Image</th>
            <th class="product-name">Product</th>
            <th class="product-price">Price</th>
            <th class="product-quantity">Quantity</th>
            <th class="product-subtotal">Subtotal</th>
        </tr>
        </thead>
        <tbody>
        <?php if ($_SESSION['cart']['products']) { ?>
            <?php foreach ($_SESSION['cart']['products'] as $item) { ?>
                <tr class="cart_item">
                    <td class="product-thumbnail">
                        <img width="145" height="145" alt="thumb-img" class="shop_thumbnail"
                             src="<?php echo asset('img/thumb-' . $item['image']) ?>">
                    </td>
                    <td class="product-name"><?php echo $item['title']; ?></td>
                    <td class="product-price">
                        <span class="amount">&euro; <?php echo round($item['price']); ?></span>
                    </td>
                    <td class="product-quantity">
                        <div class="quantity buttons_added">
                            <button type="button" class="minus remove-from-cart-modal"
                                    data-url="<?php echo router()->name('cart.removeitemmodal', ['id' => $item['id']]); ?>">
                                -
                            </button>
                            <p><?php echo trim($item['quantity']); ?></p>
                            <button type="button" class="plus add-to-cart-modal"
                                    data-url="<?php echo router()->name('cart.additemmodal', ['id' => $item['id']]); ?>">
                                +
                            </button>
                        </div>
                    </td>
                    <td class="product-subtotal">
                        <span class="amount">&euro; <?php echo $item['quantity'] * $item['price']; ?></span>
                    </td>
                </tr>
            <?php } ?>
        <?php } else { ?>
            <tr class="cart_item">
                <td class="red-color" colspan="5"><em>It seems that your cart is empty.</em> 😟</td>
            </tr>
        <?php } ?>
        <tr>
            <td class="actions" colspan="4">
                <div class="coupon">
                    <label for="coupon_code">Got a coupon?</label>
                    <input autocomplete="off" type="text" placeholder="Coupon code" value="" id="coupon_code"
                           class="input-text" name="coupon_code">
                    <button type="button" class="button">Apply Coupon</button>
                </div>
            </td>
            <td colspan="1">
                <label for="total_price">Total</label>
                <div class="total_price">&euro; <?php echo $_SESSION['cart']['total'] ?></div>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="modal-footer">
    <?php if (!empty($_SESSION['cart']['products'])) { ?>
        <button type="button" class="button reset-cart" data-url="<?php echo router()->name('cart.reset'); ?>">empty
            Cart
        </button>
        <a type="button" class="button checkout-button button alt wc-forward"
           href="<?php echo router()->name('pay'); ?>">Proceed to Checkout</a>
    <?php } ?>
</div>
