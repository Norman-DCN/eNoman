<div class="site-branding-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="logo">
                    <h1><a href="<?php echo router()->name('home'); ?>">Noman's <span>Electronics</span></a></h1>
                </div>
            </div>
        </div>
    </div>
</div>