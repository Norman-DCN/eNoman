<div class="mainmenu-area">
    <div class="container">
        <div class="row col-sm-6">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse height-fixed">
                <ul class="navigation nav navbar-nav">
                    <li><a href="<?php echo router()->name('home'); ?>">Home</a></li>
                    <li><a href="<?php echo router()->name('products'); ?>">Products</a></li>
                </ul>
            </div>
        </div>
        <div id="cart" class="col-sm-6 shopping-item-wrapper">
            <div id="cart-button">
                <?php include PATH_APP_VIEWS . 'partials/cart.html.php' ?>
            </div>
            <div class="modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="cartModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered " role="document">
                    <div class="modal-content">
                        <?php include PATH_APP_VIEWS . 'partials/cart-modal-content.html.php' ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>