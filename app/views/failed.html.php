<!DOCTYPE html>
<html lang="en">
<head>
    <?php include PATH_APP_VIEWS . 'partials/head.html.php' ?>
</head>
<body>
<?php include PATH_APP_VIEWS . 'partials/branding.html.php' ?>
<!-- End site branding area -->
<?php include PATH_APP_VIEWS . 'partials/menu.html.php' ?>
<!-- End mainmenu area -->

<div class="product-big-title-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-bit-title text-center">
                    <h2>Failed</h2>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="single-product-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-content-right">
                    <div class="row h1">
                        Sadly, the payment failed. let's go back to the <a href="<?php echo router()->name('pay') ?>">checkout</a>!
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--JavaScript library + footer -->
<?php include PATH_APP_VIEWS . 'partials/footer.html.php'; ?>
<?php include PATH_APP_VIEWS . 'partials/jslib.html.php'; ?>
</body>
</html>