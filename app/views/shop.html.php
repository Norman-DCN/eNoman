<!DOCTYPE html>
<html lang="en">
<head>
    <?php include PATH_APP_VIEWS . 'partials/head.html.php'; ?>
    <title>eNoman</title>
</head>
<body>
<?php include PATH_APP_VIEWS . 'partials/branding.html.php' ?>
<!-- End site branding area -->
<?php include PATH_APP_VIEWS . 'partials/menu.html.php' ?>
<!-- End mainmenu area -->

<div class="product-big-title-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-bit-title text-center">
                    <h2>All Products</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="single-product-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <?php foreach ($products as $product) { ?>
                <div class="col-md-3 col-sm-6">
                    <div class="single-shop-product">
                        <div class="product-upper">
                            <img src="<?php echo 'img/' . $product->image; ?>" alt="<?php $product->slug; ?>">
                        </div>
                        <h2>
                            <a href="<?php echo router()->name('product', ['slug' => $product->slug]); ?>"><?php echo $product->title; ?></a>
                        </h2>
                        <div class="product-carousel-price">
                            <ins><?php echo $product->price; ?></ins>
                        </div>

                        <div class="product-option-shop">
                            <button id="clickMe" class="add-to-cart-link"
                                    data-url="<?php echo router()->name('cart.add', ['id' => $product->id]); ?>">Add to
                                cart
                            </button>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<!--JavaScript library + footer -->
<?php include PATH_APP_VIEWS . 'partials/footer.html.php'; ?>
<?php include PATH_APP_VIEWS . 'partials/jslib.html.php'; ?>
<a class="back-top-button"></a>
</body>
</html>