<?php

abstract class Validation
{

    private $rules = [];
    private $result = [];
    private $error;
    private $validations = [
        'required',
        'number',
        'email',
        'name',
        'min',
        'max',
        'confirm',
        'postcode',
    ];

    public function __construct()
    {
        $this->rules = $this->validate();

        $this->runChecks()->get();
    }

    abstract function validate();


    private function runChecks()
    {
        foreach ($this->rules as $key => $checks) {
            foreach ($checks as $check) {
                $checkExploded = explode(':', $check);
                if (count($checkExploded) > 1) {
                    // when the check has a :. If it has, the second item in the array will be a value
                    $checkFunction = 'is' . ucfirst($checkExploded[0]);

                    if ($error = $this->$checkFunction($_POST[$key], $checkExploded[1], $key, $checks)) {
                        if (array_key_exists($key, $this->result)) {
                            array_push($this->result[$key], $error);
                        } else {
                            $this->result[$key] = [$error];
                        }
                    }

                } else {
                    // when the check has no :
                    $checkFunction = 'is' . ucfirst($check);

                    if ($error = $this->$checkFunction($_POST[$key], $key, $checks)) {
                        if (array_key_exists($key, $this->result)) {
                            array_push($this->result[$key], $error);
                        } else {
                            $this->result[$key] = [$error];
                        }
                    }
                }
            }
        }
        return $this;
    }

    /*
     *  validation functions, checking if the key and the value exist.
     *  If its not valid, they will add the error to the error list.
     */
    private function isRequired($value, $key, $checks)
    {
        if (!in_array($key, array_keys(router()->parameters)) || @router()->parameters[$key] == '') {
            $this->addErrorToResultByKey($key, $this->getError('required'));
        }
    }

    private function isName($value, $key, $checks)
    {
        if (!in_array($key, array_keys(router()->parameters)) || @router()->parameters[$key] != '') {
            if ($value && !preg_match('/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,z.\'-]+$/u', $value)) {
                $this->addErrorToResultByKey($key, $this->getError('name'));
            }
        }
    }

    private function isEmail($value, $key, $checks)
    {
        if (!in_array($key, array_keys(router()->parameters)) || @router()->parameters[$key] != '') {
            if ($value && !preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $value)) {
                $this->addErrorToResultByKey($key, $this->getError('email'));

            }
        }
    }

    private function isNumber($value, $key, $checks)
    {
        if (!in_array($key, array_keys(router()->parameters)) || @router()->parameters[$key] != '') {
            if ($value && !is_numeric($value)) {
                $this->addErrorToResultByKey($key, $this->getError('number'));
            }
        }
    }

    private function isMax($value, $amount, $key, $checks)
    {
        if (!in_array($key, array_keys(router()->parameters)) || @router()->parameters[$key] != '') {
            if($value && strlen($value) > $amount) {
                $this->addErrorToResultByKey($key, $this->getError('max'));
            }
        }
    }

    private function isMin($value, $amount, $key, $checks)
    {
        if (!in_array($key, array_keys(router()->parameters)) || @router()->parameters[$key] != '') {
            if($value && strlen($value) < $amount) {
                $this->addErrorToResultByKey($key, $this->getError('min'));
            }
        }
    }

    private function isPostcode($value, $key, $checks)
    {
        if (!in_array($key, array_keys(router()->parameters)) || @router()->parameters[$key] != '') {
            if ($value && !preg_match('/^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i', $value)) {
                $this->addErrorToResultByKey($key, $this->getError('postcode'));
            }
        }
    }

    private function isConfirm($value, $key, $rule)
    {

        if (!in_array($key . '_confirm', array_keys(router()->parameters)) || router()->parameters[$key] != router()->parameters[$key . '_confirm']) {
            $this->addErrorToResultByKey($key, $this->getError('confirm'));
        }
    }
    /*
     * add the errors we collected by key, to the result
     */
    public function addErrorToResultByKey($key, $error)
    {
        if (array_key_exists($key, $this->result)) {
            array_push($this->result[$key], $error);
        } else {
            $this->result[$key] = [$error];
        }
    }

    public function get()
    {
        if (count($this->result)) {
            router()->back(router()->parameters, $this->result);
        }
        return $this;
    }

    public function getError($rule)
    {
        return lang('validation.php')[$rule];
    }
}
