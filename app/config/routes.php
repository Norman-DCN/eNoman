<?php
/**
 * Created by PhpStorm.
 * User: Gebruiker
 * Date: 20/05/2018
 * Time: 20:54
 */
// get requests
router()->get('/', 'HomeController', 'index', 'home');
router()->get('/products', 'ProductController', 'products', 'products');
router()->get('/products/{slug}', 'ProductController', 'index', 'product');
router()->get('/pay', 'PayController', 'index', 'pay');
router()->get('/pay/done/{orderId}', 'PayController', 'done', 'pay.done');
router()->get('/pay/webhook/{orderId}', 'PayController', 'webhook', 'pay.webhook');
router()->get('/pay/success/{orderId}', 'PayController', 'success', 'pay.success');
router()->get('/pay/canceled/{orderId}', 'PayController', 'canceled', 'pay.canceled');
router()->get('/pay/failed/{orderId}', 'PayController', 'failed', 'pay.failed');
router()->get('/cart/get', 'CartController', 'get', 'cart.get');
router()->get('/cart/get/modal', 'CartController', 'getModal', 'cart.get.modal');

// post requests
router()->post('/pay/create', 'PayController', 'create', 'pay.create');
router()->post('/cart/add/{id}', 'CartController', 'add', 'cart.add');
router()->post('/cart/remove/{id}', 'CartController', 'remove', 'cart.remove');
router()->post('/cart/additemmodal/{id}', 'CartController', 'addItemModal', 'cart.additemmodal');
router()->post('/cart/removeitemmodal/{id}', 'CartController', 'removeItemModal', 'cart.removeitemmodal');
router()->post('/cart/reset', 'CartController', 'reset', 'cart.reset');
router()->post('/pay/create', 'PayController', 'create', 'pay.create');