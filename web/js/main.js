jQuery(document).ready(function ($) {
    // jQuery sticky Menu
    $(".mainmenu-area").sticky({topSpacing: 0});

    var btn = $('.back-top-button');

    $(window).scroll(function () {
        if ($(window).scrollTop() > 300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });

    btn.on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({scrollTop: 0}, '300');
    });


    $('.product-carousel').owlCarousel({
        loop: true,
        nav: true,
        margin: 20,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    });

    $('.related-products-carousel').owlCarousel({
        loop: true,
        nav: true,
        margin: 20,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 2
            },
            1200: {
                items: 3
            }
        }
    });

    $('.brand-list').owlCarousel({
        loop: true,
        nav: true,
        margin: 20,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    });


    // Bootstrap Mobile Menu fix
    $(".navbar-nav li a").click(function () {
        $(".navbar-collapse").removeClass('in');
    });

    // jQuery Scroll effect
    $('.navbar-nav li a, .scroll-to-up').bind('click', function (event) {
        var $anchor = $(this);
        var headerH = $('.header-area').outerHeight();
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - headerH + "px"
        }, 1200, 'easeInOutExpo');

        event.preventDefault();
    });

    // Bootstrap ScrollPSY
    $('body').scrollspy({
        target: '.navbar-collapse',
        offset: 95
    });
    cart();
});

function cart() {
    $('.add-to-cart-link, .remove-from-cart-link').unbind('click').click(function (event) {
        event.preventDefault();

        // alert($(this).data('url'));

        $.ajax($(this).data('url'), {
            method: 'post',
            cache: false
            // dataType: 'json'
        })
            .done(function (data) {
                if (data) {
                    $('#cart-button').html(data);
                    console.log(data);
                    cart();
                }
            })
            .fail(function () {
                alert("epic fail");
                cart();
            });
    });

    $('.add-to-cart-modal, .remove-from-cart-modal, .reset-cart').unbind('click').click(function (event) {
        event.preventDefault();

        // alert($(this).data('url'));

        $.ajax($(this).data('url'), {
            method: 'post',
            cache: false
            // dataType: 'json'
        })
            .done(function (data) {
                if (data) {
                    console.log(data);
                    $('.modal-content').html(data);
                    cart();
                }
            })
            .fail(function () {
                alert("epic fail");
                cart();
            });
    });

    $('.get-cart-modal').unbind('click').click(function (event) {
        event.preventDefault();

        $.ajax($(this).data('url'), {
            method: 'get',
            cache: false
        })
            .done(function (data) {
                $('.modal-content').html(data);
                cart();
            })
            .fail(function () {
                alert("epic fail");
                cart();
            });
    });

    $('.get-cart').unbind('click').click(function (event) {
        event.preventDefault();

        $.ajax($(this).data('url'), {
            method: 'get',
            cache: false
        })
            .done(function (data) {
                $('#cart-button').html(data);
                console.log(data);
                cart();
            })
            .fail(function () {
                alert("epic fail");
                cart();
            });
    });
}



